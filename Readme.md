
This repository  contains my submission for the Metis Data Science Bootcamp


This submission contains two folders "challenge1" and "challenge2" that contains the content of challenge 1 and 2 accordingly

###Challenge 1
I evaluated the D3 repository against R's ggplot and matplotlib with respect to the number of authors, weekly and weekday commits

I explored the dataset using an ipython notebook. The notebook can be viewed [here](http://nbviewer.ipython.org/urls/bitbucket.org/ntp5633/metis/raw/e531ec8417222c4a04fe4278e4821b32bf2f3e2e/challenge1/Repo%20Comparisions.ipynb).

If you want to run the files locally:



##### Instructions
- Navigate to the challenge 1 directory
 
		cd challenge1

- Install python3 lib dependencies if needed

		pip install requests pandas numpy matplotlib ipython

- start the ipython server
	
		ipython notebook

- view the notebook (by default port 8888)

 		http://localhost:8888
- open the notebook "Repo Comparisions"
		
		
   

###Challenge 2
My sketch of my idea can found in [idea.pdf](https://bytebucket.org/ntp5633/metis/raw/e531ec8417222c4a04fe4278e4821b32bf2f3e2e/challenge2/idea.pdf)  